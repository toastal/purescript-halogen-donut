-- SPDX-License-Identifier: BSD-3-Clause
-- Copyright © 2020 toastal purescript-halogen-donut
module Halogen.Donut (progress, spinner) where

import Prelude
import Data.Array ((:))
import Halogen.HTML as HH
import Halogen.HTML.Properties.ARIA as ARIA

svgElem ∷ ∀ q p i. String → Array (HH.IProp q i) → Array (HH.HTML p i) → HH.HTML p i
svgElem name = HH.elementNS (HH.Namespace "http://www.w3.org/2000/svg") (HH.ElemName name)

attr ∷ ∀ p i. String → String → HH.IProp p i
attr name = HH.attr (HH.AttrName name)

rootAttrs ∷ ∀ p i. String → Array (HH.IProp p i)
rootAttrs className =
  [ ARIA.role "progressbar"
  , attr "viewBox" "0 0 36 36"
  , attr "version" "1.1"
  , attr "width" "100px"
  , attr "class" ("Donut Donut--" <> className)
  ]

progress ∷ ∀ p i. Number → HH.HTML p i
progress val =
  svgElem "svg"
    (ARIA.valueNow value : rootAttrs "progress")
    [ svgElem "circle"
        [ attr "class" "Donut-wheel"
        , attr "cx" "18"
        , attr "cy" "18"
        , attr "r" "15.9155"
        ]
        []
    , svgElem "circle"
        [ attr "class" "Donut-heart"
        , attr "cx" "18"
        , attr "cy" "18"
        , attr "r" "15.9155"
        , attr "transform" "rotate(-90, 18, 18)"
        , attr "style" ("strokeDasharray:" <> value <> ",100")
        ]
        []
    ]
  where
  value ∷ String
  value
    | val < 0.0 = "0"
    | val > 1.0 = "100"
    | otherwise = show (val * 100.0)

spinner ∷ ∀ p i. HH.HTML p i
spinner =
  svgElem "svg"
    (rootAttrs "spinner")
    [ svgElem "circle"
        [ attr "class" "Donut-wheel"
        , attr "cx" "18"
        , attr "cy" "18"
        , attr "r" "15.9155"
        ]
        []
    , svgElem "circle"
        [ attr "class" "Donut-heart"
        , attr "cx" "18"
        , attr "cy" "18"
        , attr "r" "15.9155"
        ]
        []
    ]
