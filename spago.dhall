{ name = "halogen-donut"
, dependencies = [ "halogen", "prelude" ]
, packages = ./packages.dhall
, sources = [ "src/**/*.purs" ]
}
