# halogen-donut

A simple donut-shaped spinner that in a indefinite throbber state as well as a progress indicator.

## Install

Add to your contents from the [`spago.dhall`](./spago.dhall) to your `packages.dhall`. Import the styles from [`Donut.css`](./src/Halogen/Donut.css) into your own styles.

## TODO

Make a demo, because it’s pretty hard to tell to tell what this is and what it will look like.


- - -


## License

This project is licensed under the BSD 3-Clause “New” or “Revised” License - see the [LICENSE](./LICENSE) file for details.
